import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {DialogModule} from 'primeng/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {SidebarModule} from 'primeng/sidebar';
import {ToolbarModule} from 'primeng/toolbar';

import { AppComponent } from './app.component';
import { EditComponent } from './components/edit/edit.component';
import { CardModule } from 'primeng/card';
import { RouterModule, Route } from '@angular/router';
import { CreateComponent } from './components/create/create.component';
import { ListComponent } from './components/list/list.component';

import { CarsDataService } from './cars-data.service';

const routes: Route[] = [
  { path: '', component: ListComponent },
  { path: 'edit', component: EditComponent },
  { path: 'create', component: CreateComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    EditComponent,
    CreateComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    CardModule,
    RouterModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ButtonModule,
    InputTextModule,
    DialogModule,
    BrowserAnimationsModule,
    SidebarModule,
    ToolbarModule
  ],
  providers: [CarsDataService],
  bootstrap: [AppComponent]
})
export class AppModule {

}
