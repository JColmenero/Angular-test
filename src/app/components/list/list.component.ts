import { Component, OnInit } from '@angular/core';
import { CarsDataService } from '../../cars-data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  post: any = [];
  display: boolean = false;
  displaySB: boolean = false;
  editValid: boolean = false;
  createValid: boolean = false;

  constructor(private carsData: CarsDataService) {
    this.carsData.getData().subscribe(data => {
      this.post = data;
      console.log(data)
    })
  }

  ngOnInit(): void {
  }

  onDelete(value: any) {
    for (let index = 0; index < this.post.length; index++) {
      if (value == this.post[index]) {
        this.post.splice(index, 1);
      }
    }
  }

  onEdit(make: any, model: any, year: any, value: any) {
    if (make.value && model.value && year.value && value) {
      for (let index = 0; index < this.post.length; index++) {
        if (value == this.post[index].id) {
          this.post[index].make = make.value;
          this.post[index].model = model.value;
          this.post[index].year = year.value;
        }
      }
      make.value = "";
      model.value = "";
      year.value = "";
    } else {
      this.editValid = !this.editValid;
    }

    return false;
  }

  onCreate(make: any, model: any, year: any, horsePower: any, price: any, url: any) {
    if (make.value && model.value && year.value && horsePower.value && price.value && url.value) {
      this.post.push({
        id: this.post.length + 1,
        make: make.value,
        model: model.value,
        year: year.value,
        horsePower: horsePower.value,
        price: price.value,
        img_url: url.value
      });
      make.value = "";
      model.value = "";
      year.value = "";
      horsePower.value = "";
      price.value = "";
      url.value = "";
      this.displaySB = !this.displaySB
    } else {
      this.createValid = !this.createValid;
    }
  }

  showDialog() {
    this.display = !this.display;
  }

  showSideBar() {
    this.displaySB = !this.displaySB;
  }

}
