import { Component, OnInit } from '@angular/core';
import {CardModule} from 'primeng/card';
import { PrimeNGConfig } from 'primeng/api';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  public entero: number;
  public titulo: string;
  public comentario: string;


  constructor(private primengConfig: PrimeNGConfig) {
    this.entero = 25;
    this.titulo = 'Editar'
    this.comentario = 'Aquí podras editar la informacion de cada publicación.'
  }

  ngOnInit() {
    this.primengConfig.ripple = true;
  }

}
