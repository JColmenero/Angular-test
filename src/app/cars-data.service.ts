import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

interface post {
  "id": number;
  "year": number;
  "make": string;
  "model": string;
  "img_url": string;
  "horsepower": number;
  "price": number;
}

@Injectable({
  providedIn: 'root'
})
export class CarsDataService {

  constructor(private http: HttpClient) {
    console.log('service is work!')
  }

  getData() {
    return this.http.get<post[]>('https://private-anon-fc64f704af-carsapi1.apiary-mock.com/cars')
  }
}
